package com.levelup.examples;

import java.util.List;

public interface UserMenu {

    void print();

    void act(MenuItems item);

    enum MenuItems {
        SORT_BY_LASTNAME, SORT_BY_FIRSTNAME, SORT_BY_PHONE, FILTER_BY_NAME
    }

}
