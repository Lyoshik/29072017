package com.levelup.examples;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AddressBook {

    private List<User> users;

    AddressBook(List<User> users) {
        this.users = users;
    }

    public List<User> sortUsersBy(SortMethod sortMethod) {
        switch (sortMethod) {
            case BY_FIRSTNAME:
                return users.stream().sorted((user, user1) -> user.getFirstName().compareTo(user1.getFirstName()))
                        .collect(Collectors.toList());
            case BY_LASTNAME:
                return users.stream().sorted((user, user1) -> user.getLastName().compareTo(user1.getLastName()))
                        .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    public List<User> filterByName(String firstName, String lastName) {
        return users.stream()
                .filter(user -> user.getFirstName().contains(firstName) && user.getLastName().contains(lastName))
                .collect(Collectors.toList());
    }

    public void print() {

    }

}
