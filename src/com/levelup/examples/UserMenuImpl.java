package com.levelup.examples;

import java.util.List;
import java.util.Scanner;

public class UserMenuImpl implements UserMenu {

    private AddressBook addressBook;

    private UIInteractions uiInteractions;

    public UserMenuImpl(AddressBook addressBook, UIInteractions uiInteractions) {
        this.addressBook = addressBook;
        this.uiInteractions = uiInteractions;
    }

    @Override
    public void print() {

    }

    @Override
    public void act(MenuItems item) {
        switch (item) {
            case SORT_BY_FIRSTNAME:
                addressBook.sortUsersBy(SortMethod.BY_FIRSTNAME);
                addressBook.print();
                break;
            case SORT_BY_LASTNAME:
                addressBook.sortUsersBy(SortMethod.BY_LASTNAME);
                addressBook.print();
                break;
            case SORT_BY_PHONE:
                addressBook.sortUsersBy(SortMethod.BY_PHONE);
                addressBook.print();
                break;
            case FILTER_BY_NAME:
                addressBook.filterByName(uiInteractions.inputString("Enter firstname: "), uiInteractions.inputString("Enter lastname: "));
                addressBook.print();
                break;
        }
    }

}
