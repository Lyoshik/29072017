package com.levelup.examples;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AddressBookTest {

    List<User> users = Arrays.asList(
            new User("Dima", "Pinchuk", "380971344443"),
            new User("Dina", "Pinchuk", "380971344442"),
            new User("Andrey", "Pinchuk", "380987777700"),
            new User("Dima", "Pinchuk", "380971344443"),
            new User("Dima", "Pinchuk", "380971344443"),
            new User("Dima", "Pinchuk", "380971344443"),
            new User("Dima", "Baranoff", "380971344444"),
            new User("Petya", "Virus", "380968353340"),
            new User("Petya", "Petroff", "380968353340"),
            new User("Vasya", "AntiVirus", "77077777777"),
            new User("Vova", "Atom", "79007770011"),
            new User("Armen", "Zaryan", "380507777711"),
            new User("Ivan", "Ivanoff", "79058887777"),
            new User("Dias", "Satov", "77051111833"),
            new User("Evgeniy", "Tyapunoff", "380671084313"),
            new User("Evgeniy", "Tyapunoff", "380671084313"),
            new User("Evgeniy", "Sinichkin", "380675930422"),
            new User("Evgeniy", "Uev", "380505930422")
    );

    AddressBook addressBook = new AddressBook(users);

    @Test
    public void testSortUsersBy() throws Exception {
        List<User> users1 = addressBook.sortUsersBy(SortMethod.BY_FIRSTNAME);
        assertTrue("Andrey", true);
        assertEquals(users1.get(1).getFirstName(), "Armen");

        List<User> users2 = addressBook.sortUsersBy(SortMethod.BY_LASTNAME);
        assertEquals(users2.get(2).getLastName(), "Baranoff");

        List<User> users3 = addressBook.sortUsersBy(SortMethod.BY_LASTNAME);
        assertEquals(users3.get(0).getPhone(), "77077777777");
        assertTrue(users3.get(0).getPhone().contains("0"));
    }

    @Test
    public void testFilterByName() throws Exception {
        List<User> users = addressBook.filterByName("Evgeniy", "Uev");
        assertTrue(!users.isEmpty());
        assertEquals(1,users.size());
        User user = users.get(0);
        assertEquals("Evgeniy", user.getFirstName());

    }
}